# Configure AWS Credentials & Region
provider "aws" {
  region  = var.region
}

module "ecommerce_beanstalk_application"{
  source  = "cloudposse/elastic-beanstalk-application/aws"
  version = "0.4.0"
  # insert the 1 required variable here
  name    = var.application_name
}

module "ecommerce_beanstalk_environment"{
  source                             = "cloudposse/elastic-beanstalk-environment/aws"
  version                            = "0.17.0"
  namespace                          = var.application_name
  stage                              = var.stage
  name                               = var.application_name
  description                        = var.application_description
  region                             = var.region
  elastic_beanstalk_application_name = module.ecommerce_beanstalk_application.elastic_beanstalk_application_name
  instance_type                      = var.beanstalk_instance_type
  vpc_id                             = data.aws_vpc.selected.id
  application_subnets                = var.private_subnets
  solution_stack_name                = "64bit Amazon Linux 2018.03 v2.12.17 running Docker 18.06.1-ce"
}

# S3 Bucket for storing Elastic Beanstalk task definitions
resource "aws_s3_bucket" "ecommerce_beanstalk_deploys" {
  bucket = "${var.application_name}-deployments"
  acl = "private"
  force_destroy = true

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "ecommerce_beanstalk_deploys" {
  bucket = aws_s3_bucket.ecommerce_beanstalk_deploys.id

  # Block new public ACLs and uploading public objects
  block_public_acls = true

  # Retroactively remove public access granted through public ACLs
  ignore_public_acls = true

  # Block new public bucket policies
  block_public_policy = true

  # Retroactivley block public and cross-account access if bucket has public policies
  restrict_public_buckets = true

}

# Elastic Container Repository for Docker images
resource "aws_ecr_repository" "ecommerce_container_repository" {
  name = var.application_name
}

# Create instance profile
resource "aws_iam_instance_profile" "ecommerce_profile" {
  name = var.ecommerce_profile_name
  role = aws_iam_role.ecommerce_role.name
}

# Beanstalk EC2 Policy
# Overriding because by default Beanstalk does not have a permission to Read ECR
resource "aws_iam_role_policy" "ecommerce_beanstalk_ec2_policy" {
  name = "${var.application_name}-iam-eb-ec2-ecr-policy"
  role = aws_iam_role.ecommerce_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ds:CreateComputer",
        "ds:DescribeDirectories",
        "ec2:DescribeInstanceStatus",
        "logs:*",
        "ssm:*",
        "ec2messages:*",
        "ecr:GetAuthorizationToken",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetRepositoryPolicy",
        "ecr:DescribeRepositories",
        "ecr:ListImages",
        "ecr:DescribeImages",
        "ecr:BatchGetImage",
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# IAM Role
resource "aws_iam_role" "ecommerce_role" {
  name = var.ecommerce_role_name
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
