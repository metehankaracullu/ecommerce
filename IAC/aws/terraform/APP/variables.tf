data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = [var.vpc_name]
  }
}

variable "private_subnets" {
  type = "list"
}
variable "vpc_name" {}

variable "region" {
  default = "eu-central-1"
}

variable "application_name" {
  default = "ecommerce"
}

variable "application_description" {
  default = "ecommerce application for dolap.com"
}


variable "beanstalk_instance_type" {
  default = "t2.micro"
}

variable "stage" {
  default = "prod"
}

variable "ecommerce_profile_name" {
  default = "ecommerce_iam_profile"
}

variable "ecommerce_role_name" {
  default = "ecommerce_iam_role"
}