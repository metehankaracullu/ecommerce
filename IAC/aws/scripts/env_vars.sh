#!/usr/bin/env bash
export TF_VAR_application_environment=prod
export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
export TF_VAR_ACCOUNT_ID=$AWS_ACCOUNT_ID
export BACKEND_BUCKET_NAME="ecommerce-$TF_VAR_application_environment"
