#!/bin/sh
SERVICE_NAME="ecommerce"
APPLICATION_NAME="$SERVICE_NAME"
EB_BUCKET="$APPLICATION_NAME-deployments"
ENV=$APPLICATION_NAME
ECR_URL="${TF_VAR_ACCOUNT_ID}.dkr.ecr.eu-central-1.amazonaws.com"
ECR_REPO_NAME=$APPLICATION_NAME
ECR_PATH="$ECR_URL/$ECR_REPO_NAME"
TAG=$(date +%s)
ZIP=$TAG.zip

echo "================================================================================"
echo "Generating environment variables from gitlab-ci.yml...."
echo "================================================================================"
echo "SERVICE_NAME        : $SERVICE_NAME"
echo "APPLICATION_NAME    : $APPLICATION_NAME"
echo "ENVIRONMENT_NAME    : $ENV"
echo "ECR_PATH            : $ECR_PATH"
echo "ACCOUNT_ID          : $TF_VAR_ACCOUNT_ID"
echo "EB_BUCKET_NAME      : $EB_BUCKET"
echo "AWS_REGION          : $AWS_REGION"
echo "IMAGE TAG           : $TAG"
echo "BACKEND_BUCKET_NAME : $BACKEND_BUCKET_NAME"

# Configure aws cli
aws configure set region $AWS_REGION
aws configure set default.output json

if [[ $2 == "app" ]]; then
  cd  ./IAC/aws/terraform/APP
elif [[ $2 == "db" ]]; then
  cd  ./IAC/aws/terraform/DB
else
  echo "Second positional parameter is not correct.It should be app or db"
fi

if [ "$1" == "plan-infrastructure" ];then
  terraform init --backend-config "bucket=$BACKEND_BUCKET_NAME" --backend-config "key=$STATE_FILE" --backend-config "region=$AWS_REGION"
  terraform plan
  echo Plan infrastructure ended with success!

elif [ "$1" == "deploy-infrastructure" ];then
  terraform init --backend-config "bucket=$BACKEND_BUCKET_NAME" --backend-config "key=$STATE_FILE" --backend-config "region=$AWS_REGION"
  terraform apply
  echo Deploy infrastructure ended with success!

elif [ "$1" == "deploy-service" ];then
	#Find jar and copy it to current location
  find build/libs/ -type f \( -name "*.jar" -not -name "*sources.jar" \) -exec cp {} ./$SERVICE_NAME.jar \;
  mv $SERVICE_NAME.jar IAC/aws/scripts/$SERVICE_NAME.jar
	
	# Login to AWS Elastic Container Registry
  cd IAC/aws/scripts
  login=$(aws ecr get-login --region eu-central-1)
  login=`echo $login | cut -d " " -f1,2,3,4,5,6,9`
  eval $login

	# Build the image
	docker build -t $SERVICE_NAME:$TAG .
	# Tag it
	docker tag $SERVICE_NAME:$TAG $ECR_PATH:$TAG
	# Push to AWS Elastic Container Registry
	docker push $ECR_PATH:$TAG
	
	# Replace the <AWS_ACCOUNT_ID> with your ID
	sed -i='' "s/<ACCOUNT_ID>/$TF_VAR_ACCOUNT_ID/" Dockerrun.aws.json
	# Replace the <NAME> with the your name
	sed -i='' "s/<NAME>/$ECR_REPO_NAME/" Dockerrun.aws.json
	# Replace the <REGION> with the selected region
	sed -i='' "s/<REGION>/$AWS_REGION/" Dockerrun.aws.json
	# Replace the <TAG> with the your version number
	sed -i='' "s/<VERSION>/$TAG/" Dockerrun.aws.json
	
	# Zip up the Dockerrun file
  zip -r $ZIP Dockerrun.aws.json

  # Send zip to S3 Bucket
  aws s3 cp $ZIP s3://$EB_BUCKET/$ZIP

	# Create a new application version with the zipped up Dockerrun file
	echo "aws elasticbeanstalk create-application-version --application-name $APPLICATION_NAME --version-label $TAG --source-bundle S3Bucket=$EB_BUCKET,S3Key=$ZIP"
	aws elasticbeanstalk create-application-version --application-name $APPLICATION_NAME --version-label $TAG --source-bundle S3Bucket=$EB_BUCKET,S3Key=$ZIP


  # Update the environment to use the new application version
  echo "aws elasticbeanstalk update-environment --environment-name $ENV --version-label $VERSION"
  aws elasticbeanstalk update-environment --environment-name $ENV --version-label $TAG --option-settings Namespace=aws:elasticbeanstalk:application:environment,OptionName=SPRING_PROFILES_ACTIVE,Value=$TF_VAR_application_environment \
      Namespace=aws:elasticbeanstalk:application:environment,OptionName=AWS_CLIENT_KEY,Value=AWS_ACCESS_KEY_ID \
      Namespace=aws:elasticbeanstalk:application:environment,OptionName=AWS_CLIENT_SECRET,Value=AWS_SECRET_ACCESS_KEY
  echo Deploy service PREVIEW ended with success!

		
elif [ "$1" == "destroy" ];then
  terraform init --backend-config "bucket=$BACKEND_BUCKET_NAME" --backend-config "key=$STATE_FILE" --backend-config "region=$AWS_REGION"
  terraform destroy --force -auto-approve
	
	echo Destroy infrastructure ended with success!	
fi
