#!/usr/bin/env bash

region=$AWS_REGION
source ./IAC/aws/scripts/env_vars.sh
bucket="$BACKEND_BUCKET_NAME"

bucket_check=$(aws s3api head-bucket --bucket $bucket 2>&1)

if [[ -z $bucket_check ]]; then
  echo "S3 Bucket ${bucket} already exists"
elif [[ $region == "us-east-1" ]]; then
  echo "Creating s3 Bucket ${bucket}"
  aws s3api create-bucket --bucket ${bucket} --region ${region}
  sleep 5 # Wait for creating the bucket
  aws s3api put-public-access-block --bucket ${bucket} --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
  aws s3api put-bucket-encryption --bucket ${bucket} --server-side-encryption-configuration '{"Rules": [{"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}}]}'
else
  echo "Creating s3 Bucket ${bucket}"
  aws s3api create-bucket --bucket ${bucket} --region ${region} --create-bucket-configuration LocationConstraint=${region}
  echo "pre sleep"
  sleep 10 # Wait for creating the bucket
  echo "after sleep"
  aws s3api put-public-access-block --bucket ${bucket} --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
  aws s3api put-bucket-encryption --bucket ${bucket} --server-side-encryption-configuration '{"Rules": [{"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}}]}'
fi
