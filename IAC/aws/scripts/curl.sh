curl -X POST \
  http://localhost:8080/api/ecommerce/product \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"productId": "11",
	"name": "airOne",
	"price": 699.99,
	"productCategory": "sneaker"

}'

curl -X POST \
  http://localhost:8080/api/ecommerce/product \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"productId": "11",
	"name": "jordan",
	"price": 999.99,
	"productCategory": "sneaker"

}'

curl -X GET \
  http://localhost:8080/api/ecommerce/product \
  -H 'Cache-Control: no-cache'


curl -X GET \
  http://localhost:8080/api/ecommerce/product/category/sneaker \
  -H 'Cache-Control: no-cache'