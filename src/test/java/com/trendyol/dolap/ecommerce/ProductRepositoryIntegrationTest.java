package com.trendyol.dolap.ecommerce;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.trendyol.dolap.ecommerce.controller.UserController;
import com.trendyol.dolap.ecommerce.entity.Product;
import com.trendyol.dolap.ecommerce.repository.ProductRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@TestPropertySource(properties = {
        "amazon.dynamodb.endpoint=http://localhost:9393/",
        "amazon.aws.accesskey=accessKey",
        "amazon.aws.secretkey=secretKey" })
public class ProductRepositoryIntegrationTest {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);
    private DynamoDBMapper dynamoDBMapper;

    @Autowired
    private AmazonDynamoDB amazonDynamoDB;

    @Autowired
    private ProductRepository productRepository;

    @Before
    public void setup() throws Exception {
        logger.debug("Before Method setup steps IN");
        dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);

        CreateTableRequest tableRequest = dynamoDBMapper.generateCreateTableRequest(Product.class);
        tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L));
        amazonDynamoDB.createTable(tableRequest);

        dynamoDBMapper.batchDelete((List<Product>)productRepository.findAll());
        logger.debug("Before Method setup steps OUT");
    }

    @Test
    public void whenExceededCase_thenReturnAll_e2ePerspective() {
        // given

        logger.info("Test Product initialize");
        Product nike = new Product("123","airone",23.33,"ayakkabı");
        logger.info("Initializing test product...");
        productRepository.save(nike);

        // when

        logger.info("Getting all products...");
        List<Product> foundAll            = productRepository.findAll();
        logger.info("Finding by Id...");
        Optional<Product> foundOneById    = productRepository.findByProductId(nike.getProductId());
        logger.info("Finding by category...");
        List<Product> foundByCategory     = productRepository.getAllByProductCategory(nike.getProductCategory());

        // then

        logger.info("Validating steps...");
        assertThat(foundAll.size()).isGreaterThan(0);
        assertThat(foundOneById.isPresent());
        // sie set for checking last index
        assertThat(foundByCategory.get(foundByCategory.size()-1).getProductCategory())
                .isEqualTo(nike.getProductCategory());
    }

}
