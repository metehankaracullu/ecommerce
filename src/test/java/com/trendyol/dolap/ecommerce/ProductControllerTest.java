package com.trendyol.dolap.ecommerce;


import com.trendyol.dolap.ecommerce.controller.ProductController;
import com.trendyol.dolap.ecommerce.entity.Product;
import com.trendyol.dolap.ecommerce.service.product.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @Qualifier("defaultProductService")
    @MockBean
    private ProductService productService;

    @Test
    public void givenProducts_whenGetProduct_thenReturnJsonArray()throws Exception {

    Product nike = new Product("123","jordan",223.23,"ayakkabı");

    List<Product> allProduct = Arrays.asList(nike);

    given(productService.getAll()).willReturn(allProduct);

    mvc.perform(get("/api/ecommerce/product")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].name", is(nike.getName())));
    }
}
