package com.trendyol.dolap.ecommerce.config;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;

@Profile("dev")
@Configuration
@EnableDynamoDBRepositories(basePackages = "com.trendyol.dolap.ecommerce")
public class DevConfig {

    @Value("${amazon.dynamodb.endpoint}")
    private String amazonDynamoDBEndpoint;

    @Bean
    public Process dynamoDBProcess() throws IOException {
        return Runtime.getRuntime()
                .exec("java -Djava.library.path=./DynamoDBLocal_lib -jar ./db/DynamoDBLocal.jar -inMemory -port 9393");
    }

    @Bean
    public AmazonDynamoDB amazonDynamoDB(Process dynamoDBProcess) {
        if (!dynamoDBProcess.isAlive()) {
            throw new IllegalThreadStateException("DynamoDB is not alive!");
        }
        AwsClientBuilder.EndpointConfiguration endpointConfiguration =
                new AwsClientBuilder.EndpointConfiguration(amazonDynamoDBEndpoint, null);
        AmazonDynamoDBClientBuilder clientBuilder = AmazonDynamoDBClientBuilder.standard();
        clientBuilder.setEndpointConfiguration(endpointConfiguration);
        return clientBuilder.build();
    }

    @Bean
    public DynamoDB dynamoDB(AmazonDynamoDB amazonDynamoDB) {
        return new DynamoDB(amazonDynamoDB);
    }

}
