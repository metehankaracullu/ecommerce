package com.trendyol.dolap.ecommerce.service.user;

import com.trendyol.dolap.ecommerce.entity.User;
import com.trendyol.dolap.ecommerce.service.BaseService;

public interface UserService extends BaseService<User> {

}
