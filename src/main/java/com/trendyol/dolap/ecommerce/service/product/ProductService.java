package com.trendyol.dolap.ecommerce.service.product;

import com.trendyol.dolap.ecommerce.entity.Product;
import com.trendyol.dolap.ecommerce.service.BaseService;

import java.util.List;

public interface ProductService extends BaseService<Product> {
    List<Product> getAllByCategory(String category);
}
