package com.trendyol.dolap.ecommerce.service;

import java.util.List;

public interface BaseService<T> {
    T create(T t) throws RuntimeException;
    T get(String id) throws RuntimeException;
    List<T> getAll();
}
