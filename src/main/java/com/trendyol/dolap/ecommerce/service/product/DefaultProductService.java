package com.trendyol.dolap.ecommerce.service.product;

import com.trendyol.dolap.ecommerce.entity.Product;
import com.trendyol.dolap.ecommerce.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultProductService implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public DefaultProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllByCategory(String category) {
        return productRepository.getAllByProductCategory(category);
    }

    @Override
    public Product create(Product product) throws RuntimeException {
        try {
            return productRepository.save(product);
        } catch (Exception ex) {
            throw new RuntimeException(String.format("Product could not created: %s", ex.getLocalizedMessage()));
        }
    }

    @Override
    public Product get(String id) throws RuntimeException {
        Optional<Product> optionalProduct = productRepository.findByProductId(id);
        if (optionalProduct.isPresent()) return optionalProduct.get();
        else throw new RuntimeException("Requested Object Could Not Found!");
    }

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }
}
