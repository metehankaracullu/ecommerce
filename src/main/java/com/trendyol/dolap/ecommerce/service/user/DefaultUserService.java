package com.trendyol.dolap.ecommerce.service.user;

import com.trendyol.dolap.ecommerce.entity.User;
import com.trendyol.dolap.ecommerce.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public DefaultUserService(UserRepository userRepository) { this.userRepository = userRepository; }

    @Override
    public User create(User user) throws RuntimeException {
        try {
            return userRepository.save(user);
        } catch (Exception ex) {
            throw new RuntimeException(String.format("User could not created: %s", ex.getLocalizedMessage()));
        }
    }

    @Override
    public User get(String id) throws RuntimeException {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) return optionalUser.get();
        else throw new RuntimeException("Requested User Could Not Found!");
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
}
