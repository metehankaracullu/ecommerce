package com.trendyol.dolap.ecommerce.controller;

import com.trendyol.dolap.ecommerce.entity.Product;
import com.trendyol.dolap.ecommerce.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/ecommerce/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    // While same interface has different implementations the interface class wouldn't know which implementation will be used
    //That's why using Qualifier
    @Qualifier("defaultProductService")
    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Controller method for fetch all products.
     *
     * @return List of Products
     * @throws RuntimeException
     */
    @GetMapping
    public ResponseEntity<List<Product>> getAll() throws RuntimeException {
        return new ResponseEntity<>(productService.getAll(), HttpStatus.OK);
    }

    /**
     *
     * @param id product Id
     * @return Product
     * @throws RuntimeException
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<Product> getById(@PathVariable("id") String id) throws RuntimeException {
        return new ResponseEntity<>(productService.get(id), HttpStatus.OK);
    }

    /**
     * Controller method for fetch all products according to  category.
     *
     * @return List of Products
     * @throws RuntimeException
     */
    @GetMapping(value = "/category/{category}")
    public ResponseEntity<List<Product>> getAllWithCategory(@PathVariable("category") String category) throws RuntimeException {
        return new ResponseEntity<>(productService.getAllByCategory(category), HttpStatus.OK);
    }

    /**
     * Controller method for create Player, playerDto object expected as a Json Object in request body.
     *
     * @param product Product json object
     * @return created Product
     * @throws RuntimeException CustomExceptionHandler Object
     */
    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody Product product) throws RuntimeException {

        Product saved = productService.create(product);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

}
