package com.trendyol.dolap.ecommerce.controller;

import com.trendyol.dolap.ecommerce.entity.User;
import com.trendyol.dolap.ecommerce.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/ecommerce/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Qualifier("defaultUserService")
    private UserService userService;


    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     *
     * @param id
     * @return
     * @throws RuntimeException
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<User> getById(@PathVariable("id") String id) throws RuntimeException{
        return new ResponseEntity<>(userService.get(id), HttpStatus.OK);
    }

    /**
     *
     * @param user
     * @return
     * @throws RuntimeException
     */
    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) throws RuntimeException{
        return new ResponseEntity<>(userService.create(user),HttpStatus.OK);
    }

}
