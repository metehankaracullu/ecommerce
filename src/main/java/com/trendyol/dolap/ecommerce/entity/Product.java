package com.trendyol.dolap.ecommerce.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import javax.validation.constraints.Size;

@DynamoDBTable(tableName = "Product")
public class Product {
    private String productId;
    @Size(min = 3,max = 20)
    private String name;
    private double price;
    private String productCategory;

    public Product(String productId, String name, double price, String productCategory) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.productCategory = productCategory;
    }

    public Product() {}

    @DynamoDBHashKey
    @DynamoDBAutoGeneratedKey
    public String getProductId() {
        return productId;
    }

    @DynamoDBAttribute
    public String getName() {
        return name;
    }

    @DynamoDBAttribute
    public double getPrice() {
        return price;
    }

    @DynamoDBAttribute
    public String getProductCategory() {
        return productCategory;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }
}
