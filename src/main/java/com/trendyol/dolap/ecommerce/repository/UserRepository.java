package com.trendyol.dolap.ecommerce.repository;

import com.trendyol.dolap.ecommerce.entity.User;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

@EnableScan
public interface UserRepository extends CrudRepository <User, String> {
    List<User> findAll();
    Optional<User> findById(String id);
}
