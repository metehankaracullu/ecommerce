package com.trendyol.dolap.ecommerce.repository;

import com.trendyol.dolap.ecommerce.entity.Product;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

@EnableScan
public interface ProductRepository extends CrudRepository<Product, String> {
    List<Product> getAllByProductCategory(String category);
    List<Product> findAll();
    Optional<Product> findByProductId(String id);


}
