# Simple E-Commerce Application with aws Infrastructure

![AWS - INFRASTRUCTURE](dolap-ecommerce.png)

## Modules

### DockerBeanstalk
Is a base image for whole project will build on it.
Needs to build image and register gitlab Container Registry then push image to project own registry.
Gitlab push commands under Packages/Container Registry:

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/metehankaracullu/ecommerce .
docker push registry.gitlab.com/metehankaracullu/ecommerce
```

### db
Is library & classes. Requires for running dynamoDB locally.

### CI-CD Steps
![CI-CD Pipeline](pipeline.PNG)

Firstly we are building our application for taking jar files.

Then as you can see there is separate steps for APP & DB layer.
It designed for separate deployment DB and APP side.

In-case of any failover on beanstalk environment or any resource of app side we don't want to destroy or change our Database.
That's why this steps separate from each other.

And also we have 'deploy-service' job.
If you just wants to update your source code and generate new jar file;
It is enough to run build & deploy-service job.
Because deploy-service job is updating .jar file inside the docker container and re-deploy it existing beanstalk application.

### IAC

Our infrastructure designed for AWS. It has scripts and terraform modules.

configure_S3_For_Backend.sh -> generates s3 repo with awscli for locating terraform.state file.

Dockerfile -> Dockerize our builded .jar and stores on ecr

Dockerrun.aws.json -> we need this configuration file for running beanstalk environment. During the service deployment as docker we are giving this config to beanstalk environment for link to ecr repo address.

run.sh -> Our main run script for pipeline steps.  

<aside class="notice">

You must replace environment variables (Settings -> CI/CD) with your own creds.
</aside>

#### Main Resources

[BAELDUNG](https://www.baeldung.com/)

[AWS DYNAMODB DEVELOPER GUIDE](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html)

[TERRAFORM REGISTRY](https://registry.terraform.io/)
